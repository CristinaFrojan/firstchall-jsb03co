"use strict";

/*
1.

function calculator(n1, n2) {
  let elegir = prompt("Elige una operación: ");
  let suma = n1 + n2;
  let resta = n1 - n2;
  let multiplicacion = n1 * n2;
  let division = n1 / n2;
  let potencia = Math.pow(n1, n2);

  if (elegir === "suma") {
    return suma;
  } else if (elegir === "resta") {
    return resta;
  } else if (elegir === "multiplicacion") {
    return multiplicacion;
  } else if (elegir === "division") {
    return division;
  } else if (elegir === "potencia") {
    return potencia;
  }
  return "El resultado es : " + elegir;
}

console.log(calculator(4, 2));


2.


class Person {
  constructor(name, edad, genero) {
    this.name = name;
    this.edad = edad;
    this.genero = genero;
  }

  mostrarPropiedades() {
    console.log(this.name, this.edad, this.genero);
  }
}

class Teacher extends Person {
  constructor(name, edad, genero, subject) {
    super(name, edad, genero);
    this.subject = subject;
    this.studensList = [];
  }

  asignarAlumnos(student) {
    this.studensList.push(student);
  }
}

class Student extends Person {
  constructor(name, edad, genero, course, group) {
    super(name, edad, genero);
    this.course = course;
    this.group = group;
  }
}

const Cris = new Person("Cris", "25", "Femenino");
Cris.mostrarPropiedades();

const Manuel = new Student("Manuel", "20", "masculino", "1º", "B");
const Marta = new Student("Marta", "21", "femenino", "1º", "B");

const Pablo = new Student("Pablo", "22", "masculino", "2º", "B");
const Sara = new Student("Sara", "20", "femenino", "2º", "B");

const Jose = new Student("Jose", "21", "masculino", "3º", "B");
const Andrea = new Student("Andrea", "22", "femenino", "3º", "B");

const Maria = new Teacher("Maria", "35", "femenino", "historia");
const David = new Teacher("David", "40", "masculino", "lengua");
const Marcos = new Teacher("Marcos", "45", "femenino", "programación");

Maria.asignarAlumnos(Manuel);
Maria.asignarAlumnos(Marta);

David.asignarAlumnos(Pablo);
David.asignarAlumnos(Sara);

Marcos.asignarAlumnos(Jose);
Marcos.asignarAlumnos(Andrea);

console.log(Maria);
console.log(David);
console.log(Marcos);

3.



function dadoElectronico() {
  let numGenerados = [];
  let gameOver = 0;
  let final = 50;

  while (gameOver <= final) {
    let valores = Math.floor(Math.random() * 6);
    gameOver += valores;
    numGenerados.push(valores);
    console.log(gameOver);
  }
  return `Los numeros generados son:  ${numGenerados}, ¡¡¡Fin del juego, ${gameOver}!!!`;
}
console.log(dadoElectronico());
*/

